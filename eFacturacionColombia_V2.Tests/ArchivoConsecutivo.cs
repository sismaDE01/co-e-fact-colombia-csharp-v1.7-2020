﻿using System;
using System.IO;

namespace eFacturacionColombia_V2.Tests
{
    class ArchivoConsecutivo
    {
        private FileInfo _file;

        public string Ruta
        {
            get { return _file.FullName; }
        }

        public ArchivoConsecutivo(string ruta)
        {
            _file = new FileInfo(ruta);
        }


        public int ObtenerActual()
        {
            int numero = 0;

            if (_file.Exists)
            {
                int.TryParse(File.ReadAllText(_file.FullName).Trim(), out numero);
            }
            else
            {
                File.WriteAllText(_file.FullName, numero.ToString());
            }

            return numero;
        }

        public int ObtenerSiguiente()
        {
            int siguiente = this.ObtenerActual() + 1;

            File.WriteAllText(_file.FullName, siguiente.ToString());

            return siguiente;
        }

        public int Retroceder()
        {
            int anterior = this.ObtenerActual() - 1;

            File.WriteAllText(_file.FullName, anterior.ToString());

            return anterior;
        }
    }
}